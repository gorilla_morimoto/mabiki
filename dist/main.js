export const ikkai = (ms) => (fn) => {
    let id = undefined;
    return (arg) => {
        clearTimeout(id);
        id = setTimeout(() => fn(arg), ms);
    };
};
export const herasu = (ms) => (fn) => {
    let madaHayai = false;
    return (arg) => {
        if (madaHayai) {
            return;
        }
        madaHayai = true;
        fn(arg);
        setTimeout(() => {
            madaHayai = false;
        }, ms);
    };
};
