declare type Fn<T> = (arg?: T) => void;
export declare const ikkai: (ms: number) => <T>(fn: Fn<T>) => Fn<T>;
export declare const herasu: (ms: number) => <T>(fn: Fn<T>) => Fn<T>;
export {};
